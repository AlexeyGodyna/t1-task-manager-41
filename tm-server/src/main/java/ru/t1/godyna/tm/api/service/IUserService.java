package ru.t1.godyna.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.enumerated.Role;
import ru.t1.godyna.tm.model.User;

import java.util.Collection;
import java.util.List;

public interface IUserService {

    @Nullable
    User create(@Nullable String login, @Nullable String password);

    @Nullable
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @Nullable
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    Collection<User> set(@NotNull Collection<User> users);

    @NotNull
    List<User> findAll();

    @Nullable
    User findOneById(@Nullable String id);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @Nullable
    User removeByLogin(@Nullable String login);

    @Nullable
    User removeByEmail(@Nullable String email);

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    User updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    boolean isLoginExist(@Nullable String login);

    boolean isMailExist(@Nullable String email);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
