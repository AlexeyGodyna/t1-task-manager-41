package ru.t1.godyna.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.dto.request.AbstractIdRequest;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectStartByIdRequest extends AbstractIdRequest {

    public ProjectStartByIdRequest(
            @Nullable final String token,
            @Nullable final String id
    ) {
        super(token, id);
    }

}
